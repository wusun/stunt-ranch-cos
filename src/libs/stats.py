"""Statistical functions."""
from collections import namedtuple

import numpy as np


def hourly_median(hours, series, all_hours=True):
    """
    Calculate hourly binned medians of a time series.

    Parameters
    ----------
    hours : array_like
        Time series of the hour number. Must be of the same length as `series`.
    series : array_like
        Time series of the data.
    all_hours : bool, optional
        Default is `True` to consider 24 hours. If `False`, only consider the
        hours that are present in the `hours` input.

    Returns
    -------
    hour_level : array_like
        Hour levels.
    median : array_like
        Median values by the hour.
    q1 : array_like
        First quartile values by the hour.
    q3 : array_like
        Third quartile values by the hour.

    See Also
    --------
    `hourly_avg` : Hourly binned average function.

    """
    if all_hours:
        hour_level = np.arange(24)
    else:
        hour_level = np.unique(hours)
    med_hr = np.zeros(hour_level.size) + np.nan
    q1_hr = np.zeros(hour_level.size) + np.nan
    q3_hr = np.zeros(hour_level.size) + np.nan
    for i in range(hour_level.size):
        med_hr[i] = np.nanmedian(series[hours == hour_level[i]])
        q1_hr[i], q3_hr[i] = np.nanpercentile(
            series[hours == hour_level[i]], [25.0, 75]
        )

    HourlyMedianResult = namedtuple(
        "HourlyMedianResult", ("hour_level", "median", "q1", "q3")
    )
    return HourlyMedianResult(hour_level, med_hr, q1_hr, q3_hr)


def pvalue_to_asterisks(p):
    """Represent p-values in significance notations with asterisks."""
    if p < 0:
        raise ValueError("p-value must not be negative!")
    if p <= 1e-4:
        return "****"
    elif p <= 1e-3:
        return "****"
    elif p <= 1e-2:
        return "**"
    elif p <= 0.05:
        return "*"
    else:
        return "n.s."


def fp2decimal(x):
    """Convert floating point numbers to decimal scientific notation."""
    log10x = np.log10(x)
    exponent = np.floor(log10x).astype("int")
    mantissa = x * 10 ** (-exponent)
    sign = "" if x >= 0 else "-"
    return f"{sign}{mantissa:.2g}\\times " + "10^{%d}" % exponent
