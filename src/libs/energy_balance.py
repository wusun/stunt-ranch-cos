"""Leaf energy balance."""
from scipy import constants, optimize

cp_dryair = 29.080358  # specific heat capacity of dry air [J mol^-1 K^-1]


def latent_heat_vap(temp):
    """Calculate latent heat of vaporization [J mol^-1]."""
    return 1.91846e6 * (temp / (temp - 33.91)) ** 2 * 18.015_28e-3


def solve_leaf_temp(
    ppfd, temp_air, f_h2o, g_bh, albedo=0.1, emissivity=0.95, solver="newton"
):
    """Solve leaf temperature."""

    def _energy_imbalance(temp_leaf):
        # shortwave radiation [W m^-2]
        R_sw = ppfd * 0.5 * (1 - albedo)
        # outgoing longwave radiation [W m^-2]
        R_lw = 2 * emissivity * constants.sigma * temp_leaf**4
        # latent heat [W m^-2]
        LE = latent_heat_vap(temp_leaf) * f_h2o
        # sensible heat [W m^-2]
        SH = 2 * cp_dryair * g_bh * (temp_leaf - temp_air)
        if solver == "newton":
            return R_sw - R_lw - LE - SH
        else:
            return 0.5 * (R_sw - R_lw - LE - SH) ** 2

    if solver == "newton":
        return optimize.newton(_energy_imbalance, temp_air)
    else:
        res = optimize.minimize(_energy_imbalance, temp_air, method="BFGS")
        if res.success:
            return res.x[0]
        else:
            return np.nan
