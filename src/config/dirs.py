# Stunt Ranch Leaf Flux Project 2013
#
# Copyright (c) 2023 Wu Sun <wsun@carnegiescience.edu>
# Department of Global Ecology
# Carnegie Institution for Science
"""Directory settings."""
import os

# project root directory
root: str = os.path.abspath(os.path.join(os.path.dirname(__file__), "../.."))

# source files
src: str = f"{root}/src"

# plots
plots: str = f"{root}/plots"

# data
data: str = f"{root}/data"
