# Leaf carbonyl sulfide flux data from Stunt Ranch in 2013

[![doi](https://zenodo.org/badge/doi/10.5281/zenodo.13362965.svg)](https://doi.org/10.5281/zenodo.13362965)

Wu Sun (<wsun@carnegiescience.edu>)

Department of Global Ecology, Carnegie Institution for Science

Updated on 2024-08-22

This repository contains data and code used to reproduce results in the
following work:

Sun, W., Maseyk, K., Lett, C., & Seibt, U. (2024). Restricted internal
diffusion weakens transpiration–photosynthesis coupling during heatwaves:
Evidence from leaf carbonyl sulphide exchange. _Plant, Cell & Environment_,
_47_(5), 1813–1833. <https://doi.org/10.1111/pce.14840>

<details>
  <summary>BibTeX citation</summary>

```bibtex
@article{sun_et_al_2024_pce,
  author   = {Wu Sun and Kadmiel Maseyk and C{\'e}line Lett and Ulli Seibt},
  title    = {Restricted internal diffusion weakens transpiration\textendash{}photosynthesis coupling during heatwaves: Evidence from leaf carbonyl sulphide exchange},
  year     = {2024},
  journal  = {Plant, Cell \& Environment},
  volume   = {47},
  number   = {5},
  pages    = {1813--1833},
  doi      = {10.1111/pce.14840},
  url      = {https://onlinelibrary.wiley.com/doi/abs/10.1111/pce.14840},
}
```
</details>

## Data

Data files are provided in the [`data`](./data) folder.

```text
data
├── fcos-blank-fit-coefs.csv
├── fcos-blank-fit-pred-lc.csv
├── fcos-blank-fit-pred.csv
├── gb.nc
├── gbh_temp.nc
├── light-responses.csv
├── stunt-ranch-chamber-fluxes-2013.csv
├── stunt-ranch-daily-leaf-fluxes-2013.csv
├── stunt-ranch-met-meta.json
├── stunt-ranch-met.txt
└── tmax-stats.json
```

### Fitting diagnostics for blank chamber effects

* [`fcos-blank-fit-coefs.csv`](./data/fcos-blank-fit-coefs.csv): Fitted
  coefficients.
* [`fcos-blank-fit-pred.csv`](./data/fcos-blank-fit-pred.csv): Fitted blank
  chamber effects.
  + `temp`: Air temperature inside the chamber.
  + `fit`: Fitted blank chamber COS effects.
  + `lwr`, `upr`: Lower and upper bounds of the 95% prediction intervals for
    the fitted blank chamber effects.
* [`fcos-blank-fit-pred-lc.csv`](./data/fcos-blank-fit-pred-lc.csv): Predicted
  blank chamber effects for leaf chambers (LC1 and LC2).
  + `ch_no`: Chamber number.
  + `temp`: Air temperature inside the chamber.
  + `fcos_blank`: Predicted potential blank chamber effects.
  + `fcos_blank_lwr`, `fcos_blank_upr`: Lower and upper bounds of the 95%
    prediction intervals for the predicted blank chamber effects.

### Leaf boundary layer conductances

* [`gb.nc`](./data/gb.nc): Leaf boundary layer conductances to heat and water
  vapor at different wind speeds and leaf characteristic lengths.
* [`gbh_temp.nc`](./data/gbh_temp.nc): Leaf boundary layer conductance to heat
  at different air temperatures.

### Leaf-level light response measurements

[`light-responses.csv`](./data/light-responses.csv): LI-6400 light response
measurements. For an explanation of the column names, refer to Table 3-2 in the
online manual of the
[LI-6400/XT Portable Photosynthesis System](https://www.licor.com/env/support/LI-6400/manuals.html).

### Leaf chamber flux measurements

[`stunt-ranch-chamber-fluxes-2013.csv`](./data/stunt-ranch-chamber-fluxes-2013.csv):
Chamber flux measurements.

* `timestamp`, `timestamp_utc`: Timestamps in local time (Pacific Daylight
  Time, UTC–0700) and UTC respectively, formatted according to the
  [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html)
  standard (`YYYY-mm-dd HH:MM:SS`).
* `doy_utc`, `doy_local`: Day of year (DOY) values in UTC and local time
  (UTC–0700), respectively.

  Note: DOY values are _floating numbers_ indicating days elapsed since
  2013-01-01 00:00:00 and are always smaller than the corresponding integer DOY
  values. For example, the fractional DOY value for 2013-01-31 12:00:00 is
  30.5, whereas the corresponding integer DOY value is 31.
* `hour`: Hour of the day (0–24) in local time.
* `ch_no`, `ch_label`: Chamber numbers and labels.
  + 1 or "LC1": Sunlit leaf chamber.
  + 2 or "LC2": Shaded leaf chamber.
  + 3 or "LC3": Blank leaf chamber.
  + 4 or "SC1": Soil chamber 1.
  + 5 or "SC2": Soil chamber 2.
  + 6 or "SC3": Blank soil chamber.

  Note: Soil chambers (SC1, SC2, and SC3) are not used in this study. See
  [Sun et al. (2016) _JGR Biogeosciences_](https://doi.org/10.1002/2015JG003149)
  for information on soil fluxes.
* `A_ch`: Chamber surface area, m<sup>2</sup>.
  + For LC1 and LC2: total leaf area.
  + For LC3: total wall surface area of the chamber.
  + For SC1, SC2, and SC3: chamber footprint area.
* `V_ch`: Chamber volume, m<sup>3</sup>.
* `{cos|co2|h2o}_{atmb|chb|cha|atma}`, `sd_{cos|co2|h2o}_{atmb|chb|cha|atma}`:
  Mean and standard deviation of COS, CO<sub>2</sub>, or H<sub>2</sub>O
  concentration during a measurement period. Units differ by gas species:
  + COS: pmol mol<sup>–1</sup>.
  + CO<sub>2</sub>: μmol mol<sup>–1</sup>.
  + H<sub>2</sub>O: mmol mol<sup>–1</sup>.

  Different measurement periods are:
  + `atmb`: Ambient air, before flux measurement.
  + `chb`: Chamber air, before flux measurement.
  + `cha`: Chamber air, after flux measurement.
  + `atma`: Ambient air, after flux measurement.
* `{cos|co2|h2o}_chc_iqr`: Interquartile range of COS, CO<sub>2</sub>, or
  H<sub>2</sub>O concentration during chamber closure for flux measurement.
  Units are pmol mol<sup>–1</sup>, μmol mol<sup>–1</sup>, and mmol
  mol<sup>–1</sup> for COS, CO<sub>2</sub>, and H<sub>2</sub>O measurements,
  respectively.
* `f{cos|co2|h2o}_{lin|rlin|nonlin}`, `se_f{cos|co2|h2o}_{lin|rlin|nonlin}`:
  Estimates and standard errors of COS, CO<sub>2</sub>, or H<sub>2</sub>O
  fluxes using different fitting methods.

  Units differ by gas species:
  + COS: pmol m<sup>–2</sup> s<sup>–1</sup>.
  + CO<sub>2</sub>: μmol m<sup>–2</sup> s<sup>–1</sup>.
  + H<sub>2</sub>O: mmol m<sup>–2</sup> s<sup>–1</sup>.

  Fitting methods are:
  + `lin`: Linear fit following [Sun et al. (2018) _Biogeosciences_](https://doi.org/10.5194/bg-15-3277-2018).
  + `rlin`: Robust linear fit.
  + `nonlin`: Exponential fit with a variable starting time.
* `qc_{cos|co2|h2o}`: Quality control flags for COS, CO<sub>2</sub>, or
  H<sub>2</sub>O fluxes, which check the consistency of flux estimates across
  fitting methods. Decisions to filter or retain data should not be based on
  these flags.
* `n_obs_{cos|co2|h2o}`: Number of COS, CO<sub>2</sub>, or H<sub>2</sub>O
  concentration observations used to calculate the flux.
* `flow_lpm`: Inflow rate, liter per minute under ambient conditions.
* `t_turnover`: Turnover time in seconds.
* `t_lag_nom`: Nominal time lag in the sampling line in seconds.
* `t_lag_optmz`: Optimized time lag in the sampling line in seconds.
* `status_tlag`: Status of time lag optimization: 0 - success; 1 - failure to
  converge.
* `pres`: Ambient pressure, Pa.
* `T_log`: Datalogger temperature, °C.
* `T_inst`: Gas analyzer instrument temperature, °C.
* `T_atm`: Ambient air temperature, °C.
* `RH_atm`: Relative humidity in the ambient air, dimensionless.
* `T_ch_{1..6}`: Air temperature inside the chambers, °C.
* `T_dew_ch`: Dew point temperature inside the chamber when fluxes were being
  measured, °C.
* `T_soil_ch_{4..6}`: Soil temperature in the top 5 cm near soil chambers, °C.
* `w_soil_ch_{4..6}`: Volumetric soil water content in the top 5 cm near soil
  chambers, m<sup>3</sup> m<sup>–3</sup>.
* `PAR_ch_{1..4}`: Photosynthetically active radiation near the chambers, μmol
  m<sup>–2</sup> s<sup>–1</sup>.
* `flag_{cos|co2|h2o}`: Combined quality flags for COS, CO<sub>2</sub>, or
  H<sub>2</sub>O concentration measurements.
* `solar_elevation_angle`: Solar elevation angle, degree.
* `daytime_indicator`: Daytime indicator: 1 - daytime; 0 - nighttime.
* `fcos_blank_lc`: Potential blank chamber COS effects for leaf chambers (LC1
  and LC2), pmol m<sup>–2</sup> s<sup>–1</sup>.
* `fcos_blank_lc.lwr`, `fcos_blank_lc.upr`: Lower and upper bounds of the 95%
  prediction intervals for the blank chamber effects in leaf chambers (LC1 and
  LC2), pmol m<sup>–2</sup> s<sup>–1</sup>.
* `fcos_lc_corrected`: Leaf COS fluxes with blank chamber effects removed, pmol
  m<sup>–2</sup> s<sup>–1</sup>.
* `se_fcos_lc_corrected`: Standard error of the corrected leaf COS fluxes, pmol
  m<sup>–2</sup> s<sup>–1</sup>.
* `fcos_lc_corrected.lwr`, `fcos_lc_corrected.upr`: Lower and upper bounds of
  the 95% prediction intervals for the corrected leaf COS fluxes, pmol
  m<sup>–2</sup> s<sup>–1</sup>.
* `heatwave_indicator`: Heatwave indicator: 1 - heatwaves; 0 - normal days.
* `vpd_atm`: Vapor pressure deficit in the ambient air, Pa.
* `e_sat_ch`: Saturation water vapor pressure inside the chamber, Pa.
* `vpd_ch`: Vapor pressure deficit inside the chamber, Pa.
* `rh_ch`: Relative humidity inside the chamber, dimensionless.
* `mfvd_atm`: Mole-fraction vapor deficit in the atmosphere, mmol
  H<sub>2</sub>O mol<sup>–1</sup> air.
* `mfvd_ch`: Mole-fraction vapor deficit inside the chamber, mmol
  H<sub>2</sub>O mol<sup>–1</sup> air.
* `g_sw`: Stomatal conductance to water vapor, mol m<sup>–2</sup>
  s<sup>–1</sup>.
* `ci_ca_ratio`: Ratio between intercellular and ambient CO<sub>2</sub>
  concentrations, for leaf chambers only.
* `g_is`: Internal conductance to COS, mol m<sup>–2</sup> s<sup>–1</sup>.
* `water_use_efficiency`: Water use efficiency, mmol CO<sub>2</sub>
  mol<sup>–1</sup> H<sub>2</sub>O.
* `lru`: Leaf COS : CO<sub>2</sub> relative uptake ratio, dimensionless.
* `lru_cos_h2o`: Leaf relative uptake ratio between COS uptake and
  transpiration, dimensionless.

[`stunt-ranch-daily-leaf-fluxes-2013.csv`](./data/stunt-ranch-daily-leaf-fluxes-2013.csv):
Daily aggregated chamber fluxes and related diagnostics.

* `heatwave_indicator`: Heatwave indicator: `True` - heatwaves; `False` -
  normal days.
* `day_length`: Day length in hours.
* `f{cos|co2}_{ch1|ch2}.{mean|sd|n_obs}`: Daily mean (`mean`), standard
  deviation (`sd`), or number of valid observations (`n_obs`) of COS or
  CO<sub>2</sub> fluxes in leaf chamber LC1 (`ch1`) or LC2 (`ch2`).
* `f{cos|co2}_{dt|nt}_{ch1|ch2}.{mean|sd|n_obs}`: Similar to the daily
  statistics for fluxes, but for either the daytime (`dt`) or nighttime (`nt`)
  subset.
* `{cos|co2}_{ch1|ch2}.{mean|sd|n_obs}`: Daily mean (`mean`), standard
  deviation (`sd`), or number of valid observations (`n_obs`) of COS or
  CO<sub>2</sub> concentration in leaf chamber LC1 (`ch1`) or LC2 (`ch2`).
* `{cos|co2}_{dt|nt}_{ch1|ch2}.{mean|sd|n_obs}`: Similar to the daily
  statistics for concentrations, but for either the daytime (`dt`) or nighttime
  (`nt`) subset.
* `lru_avg_dt_{ch1|ch2}`: Daytime integrated leaf relative uptake ratio for
  chamber LC1 (`ch1`) or LC2 (`ch2`).
* `lru_avg_{ch1|ch2}`: Daily integrated leaf relative uptake ratio for chamber
  LC1 (`ch1`) or LC2 (`ch2`).
* `fcos_nt_frac_{ch1|ch2}`: Fraction of nighttime COS uptake in daily
  integrated COS uptake for chamber LC1 (`ch1`) or LC2 (`ch2`).

### Meteorological data

Meteorological data from the Stunt Ranch weather station during the campaign
period were retrieved from the Western Regional Climate Center at
<https://wrcc.dri.edu/weather/ucsr.html>. This data set is included here for
reproducing results presented in the study only. For data use policy, please
visit the [Western Regional Climate Center](https://wrcc.dri.edu/).

* [`stunt-ranch-met-meta.json`](./data/stunt-ranch-met-meta.json): Metadata
  information for parsing by Pandas.
* [`stunt-ranch-met.txt`](./data/stunt-ranch-met.txt): Meteorological data at
  Stunt Ranch during March 24 to May 20, 2013.

### Climate statistics

* [`tmax-stats.json`](./data/tmax-stats.json): April and May maximum
  temperature statistics for Stunt Ranch during 1981–2010, extracted from the
  [PRISM Gridded Climate Data](https://prism.oregonstate.edu/) provided by the
  PRISM Climate Group, Oregon State University.

## Code

The Python codebase for reproducing the figures is in [`src`](./src).

```text
src
├── config
│   ├── __init__.py
│   └── dirs.py
├── libs
│   ├── __init__.py
│   ├── energy_balance.py
│   ├── light_response.py
│   ├── sat_vap.py
│   ├── solar_angle.py
│   ├── stats.py
│   └── stom_cond.py
└── make_plots.ipynb
```

* [`config/dirs.py`](./src/config/dirs.py): Directory settings.
* [`libs/energy_balance.py`](./src/libs/energy_balance.py): Leaf energy
  balance.
* [`libs/light_response.py`](./src/libs/light_response.py): Empirical light
  response of photosynthesis.
* [`libs/sat_vap.py`](./src/libs/sat_vap.py): Saturation vapor pressure of
  water.
* [`libs/solar_angle.py`](./src/libs/solar_angle.py): Calculates solar angles
  from coordinates and time.
* [`libs/stats.py`](./src/libs/stats.py): Miscellaneous statistical functions.
* [`libs/stom_cond.py`](./src/libs/stom_cond.py): Stomatal conductance-related
  functions.
* [`make_plots.ipynb`](./src/make_plots.ipynb): Jupyter Notebook for creating
  figures.

Before running the code, please make sure that the following prerequisites are
installed:

* Python >= 3.9 (Python 2.x not supported)
* [Jupyter Notebook](https://jupyter.org)
* [numpy](https://numpy.org)
* [scipy](https://scipy.org)
* [pandas](https://pandas.pydata.org)
* [xarray](https://xarray.dev)
* [statsmodels](https://www.statsmodels.org)
* [scikit-learn](https://scikit-learn.org)
* [matplotlib](https://matplotlib.org)
* [seaborn](https://seaborn.pydata.org)
* [windrose](https://github.com/python-windrose/windrose)

## License

This repository is licensed under either the [MIT License](./LICENSE) or the
[CC BY 4.0 License](./LICENSE-CC-BY). You may choose the license that best
suits your intended use case. By accepting the license, you are granted
permission to use this repository without requiring additional written consent
from the authors. If you use this data set, we kindly ask that you give fair
credit to the authors by citing or acknowledging this work as appropriate.

`SPDX-License-Identifier: CC-BY-4.0 OR MIT`

Please contact Wu Sun (<wsun@carnegiescience.edu>) if you have any questions.
